// bài 1

function tinhtien(){
    var tienLuong = document.getElementById("txt-luong").value*1;
    var gioLam = document.getElementById("txt-gio").value*1;
    var luong = tienLuong * gioLam;
    document.getElementById("result1").innerHTML = ` <h2 class="mt-5 text-danger text-center">
    Số Tiền Lương là: ${ luong }
    </h2>`
}

// bài 2

function trungbinh(){
    var num1 = document.getElementById("no1").value*1;
    var num2 = document.getElementById("no2").value*1;
    var num3 = document.getElementById("no3").value*1;
    var num4 = document.getElementById("no4").value*1;
    var num5 = document.getElementById("no5").value*1;
    var trungBinh = (num1 +num2+num3 +num4+ num5)/5;

    document.getElementById("result2").innerHTML = ` <h2 class="mt-5 text-danger text-center">
    Giá trị trung bình là: ${ trungBinh }
    </h2>`
}

// bài 3

function tien(){
    var tienUsd = document.getElementById("txt-tien-usd").value*1;
    var tieVnd = tienUsd * 23500;
    document.getElementById("result3").innerHTML = ` <h2 class="mt-5 text-danger text-center">
    Tiền bạn có được là: ${ tieVnd } VNĐ
    </h2>`
}

// bài 4

function tinhcv(){
    var chieuDai = document.getElementById("txt-chieu-dai").value*1;
    var chieuRong = document.getElementById("txt-chieu-rong").value*1;
    document.getElementById("result4").innerHTML = ` <h2 class="mt-5 text-danger text-center">
    Chu vi là: ${ (chieuDai + chieuRong)*2 } </br>
    Diện tích là: ${ chieuDai * chieuRong }
    </h2>`
}

// bài 5

function tinhtong(){
    var soEl = document.getElementById ("txt-so").value*1;
    var donVi = soEl % 10;
    var hangChuc = Math.floor( soEl/10);
    document.getElementById("result5").innerHTML = ` <h2 class="mt-5 text-danger text-center">
    Tổng là: ${ donVi + hangChuc }
    </h2>`
}